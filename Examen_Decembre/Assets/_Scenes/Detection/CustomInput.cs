﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CustomInput
{
    public static class CustomInput
    {
        static Dictionary<string, float> inputs = new Dictionary<string, float>();

        /// <summary>
        /// Return axis value of Custom input
        /// </summary>
        /// <param name="_axis"></param>
        /// <returns>axis value</returns>
        static public float GetAxis(string _axis)
        {
            if (!inputs.ContainsKey(_axis))
            {
                inputs.Add(_axis, 0);
            }

            return inputs[_axis];
        }

        /// <summary>
        /// Set axis value of custom input
        /// </summary>
        /// <param name="_axis"></param>
        /// <param name="_value"></param>
        static public void SetAxis(string _axis, float _value)
        {
            if (!inputs.ContainsKey(_axis))
            {
                inputs.Add(_axis, 0);
            }

            inputs[_axis] = _value;
        }

        /// <summary>
        /// Add axis with a defined value
        /// </summary>
        /// <param name="_axis"></param>
        /// <param name="_value"></param>
        static public void AddAxis(string _axis, float _value)
        {
            if (!inputs.ContainsKey(_axis))
            {
                inputs[_axis] = _value;
            }
        }
        /// <summary>
        /// Remove axis by name
        /// </summary>
        /// <param name="_axis"></param>
        /// <param name="_value"></param>
        static public void RemoveAxis(string _axis, float _value)
        {
            if (inputs.ContainsKey(_axis))
            {
                inputs.Remove(_axis);
            }
        }
    }
}
