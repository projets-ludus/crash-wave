﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomInput;
using OpenCVForUnity.UnityUtils.Helper;
using OpenCVForUnityExample;
using Rect = OpenCVForUnity.CoreModule.Rect;
public class AddingCustomInputs : MonoBehaviour
{
    //public float rapportZoneCentrale = 0.3f;
    public float headAxisValue;
    public int cameraWidth = 2000;
    public float jump;
    private int screenWidth;
    private int screenHeight;
    public int cameraHeight;
    public float movementScale = 0.1f;
    public float movementDurationAnalyseInSecond = 1;

    private float RightHand_X;
    private float RightHand_Y;
    private float LeftHand_X;
    private float LeftHand_Y;

    public float rapportZoneCentrale=0.3f;
    public float positionXTete;
    public float limiteGauche;
    public float limiteDroite;

    public List<float> EtatsPrecedent;
    private float DernierEtatTete = 0;
    private int fps;

    // Start is called before the first frame update
    void Start()
    {
        EtatsPrecedent.Add(0);
        EtatsPrecedent.RemoveAt(0);
        cameraHeight = Mathf.RoundToInt(cameraWidth * (9f / 16f));
        CustomInput.CustomInput.AddAxis("Turn",0); //ajout de l'action de la tête
        CustomInput.CustomInput.AddAxis("RightHand_Y", 0); //ajout de l'action de la main droite, en y
        CustomInput.CustomInput.AddAxis("RightHand_X", 0); //ajout de l'action de la main droite, en x
        CustomInput.CustomInput.AddAxis("LeftHand_Y", 0); //ajout de l'action de la main gauche, en y
        CustomInput.CustomInput.AddAxis("LeftHand_X", 0); //ajout de l'action de la main gauche, en x

    }

    private void Update()
    {
        screenWidth = gameObject.GetComponent<WebCamTextureToMatHelper>().GetWidth();
        screenHeight = gameObject.GetComponent<WebCamTextureToMatHelper>().GetHeight();

        if (gameObject.GetComponent<HandTracking>().enabled)
        {
            UpdateRightHandAxis();
            RightHand_Y = CustomInput.CustomInput.GetAxis("RightHand_Y");
            RightHand_X = CustomInput.CustomInput.GetAxis("RightHand_X");
        }

        if (gameObject.GetComponent<HandTrackingCopie>().enabled)
        {
            UpdateLeftHandAxis();
            LeftHand_X = CustomInput.CustomInput.GetAxis("LeftHand_X");
            LeftHand_Y = CustomInput.CustomInput.GetAxis("LeftHand_Y");
        }

        //for debugging

        if (gameObject.GetComponent<HeadTracking>().enabled)
        {
            UpdateHeadAxis();
            //UpdateHeadTrackingActionInput();
            UpdateHeadActionInput();
        }

    }

    private void UpdateRightHandAxis()
    {
        Rect hand = gameObject.GetComponent<HandTracking>().hand;
        float value;
        float axisValue;

        //main droite X
        value = (hand.x + (hand.width * 0.5f));
        if(value < (screenWidth * 0.5f))
        {
            if(value != 0)
            {
                axisValue = (1- value / (screenWidth * 0.5f))*-1;
            }
            else
            {
                axisValue = -1;
            }
        }
        else
        {
            axisValue = ((value - (screenWidth * 0.5f)) / (screenWidth * 0.5f));
        }
        CustomInput.CustomInput.SetAxis("RightHand_X", axisValue);

        //main droite Y
        value = (hand.y + (hand.height * 0.5f));
        if (value < (screenHeight * 0.5f))
        {
            if (value != 0)
            {
                axisValue = (1 - value / (screenHeight * 0.5f)) * -1;
            }
            else
            {
                axisValue = -1;
            }
        }
        else
        {
            axisValue = ((value - (screenHeight * 0.5f)) / (screenHeight * 0.5f));
        }
        CustomInput.CustomInput.SetAxis("RightHand_Y", axisValue);

    }

    private void UpdateLeftHandAxis()
    {
        Rect hand = gameObject.GetComponent<HandTrackingCopie>().hand;
        float value;
        float axisValue;

        //main gauche X
        value = (hand.x + (hand.width * 0.5f));
        if (value < (screenWidth * 0.5f))
        {
            if (value != 0)
            {
                axisValue = (1 - value / (screenWidth * 0.5f)) * -1;
            }
            else
            {
                axisValue = -1;
            }
        }
        else
        {
            axisValue = ((value - (screenWidth * 0.5f)) / (screenWidth * 0.5f));
        }
        CustomInput.CustomInput.SetAxis("LeftHand_X", axisValue);

        //main droite Y
        value = (hand.y + (hand.height * 0.5f));
        if (value < (screenHeight * 0.5f))
        {
            if (value != 0)
            {
                axisValue = (1 - value / (screenHeight * 0.5f)) * -1;
            }
            else
            {
                axisValue = -1;
            }
        }
        else
        {
            axisValue = ((value - (screenHeight * 0.5f)) / (screenHeight * 0.5f));
        }
        CustomInput.CustomInput.SetAxis("LeftHand_Y", axisValue);

    }

    private void UpdateHeadActionInput()
    {
        fps = Mathf.RoundToInt(1f / Time.deltaTime); //récupère le nombre de fps
        if (EtatsPrecedent.Count == 0)
        {
            CustomInput.CustomInput.SetAxis("Jump", 0);
        }

        Rect Head = gameObject.GetComponent<HeadTracking>().head;

        //Debug.Log("Hand Detected");
        DernierEtatTete = (Head.y + (Head.y + Head.height * 0.5f));
               
        if (EtatsPrecedent.Count > 1) //vérifie que la liste d'état précédent n'est pas nulle
        {
            DernierEtatTete = EtatsPrecedent[EtatsPrecedent.Count - 1];
        }
                    
        EtatsPrecedent.Add(DernierEtatTete);

        if ((EtatsPrecedent.Count > (fps * movementDurationAnalyseInSecond)))
        {
            //Debug.Log("Input done");
            if ((Head.y + (Head.y + Head.height * 0.5f)) < (EtatsPrecedent[0] + (cameraWidth * movementScale)))
            {
                CustomInput.CustomInput.SetAxis("Jump", 1); //turn right
                EtatsPrecedent.Clear();
            }
            else
            {
                EtatsPrecedent.RemoveAt(0); //supprime le premier élément de la liste
            }
            

            jump += CustomInput.CustomInput.GetAxis("Jump");
            
        }

    }

    private void UpdateHeadTrackingActionInput()
    {
        fps = Mathf.RoundToInt(1f / Time.deltaTime); //récupère le nombre de fps
        if (EtatsPrecedent.Count == 0)
        {
            CustomInput.CustomInput.SetAxis("Turn", 0);
        }

        Rect Head = gameObject.GetComponent<HeadTracking>().head;
        if (gameObject.GetComponent<HeadTracking>().headTrackedOnce)
        {
            //Debug.Log("Head Detected");
            DernierEtatTete = (Head.x + (Head.x + Head.width * 0.5f));
        }
        else
        {
            if (EtatsPrecedent.Count > 1) //vérifie que la liste d'état précédent n'est pas nulle
            {
                DernierEtatTete = EtatsPrecedent[EtatsPrecedent.Count - 1];
            }

        }
        EtatsPrecedent.Add(DernierEtatTete);



        if ((EtatsPrecedent.Count > (fps * movementDurationAnalyseInSecond)) && (gameObject.GetComponent<HeadTracking>().headTrackedOnce))
        {
            //Debug.Log("Input done");
            if ((Head.x + (Head.x + Head.width * 0.5f)) > (EtatsPrecedent[0] + (cameraWidth * movementScale)))
            {
                CustomInput.CustomInput.SetAxis("Turn", 1); //turn right
                EtatsPrecedent.Clear();
            }
            else if ((Head.x + (Head.x + Head.width * 0.5f)) < (EtatsPrecedent[0] - (cameraWidth * movementScale)))
            {
                CustomInput.CustomInput.SetAxis("Turn", -1); //turn left
                EtatsPrecedent.Clear();
            }
            else
            {
                EtatsPrecedent.RemoveAt(0); //supprime le premier élément de la liste
            }

            //for debugging
            if (CustomInput.CustomInput.GetAxis("Turn") != 0)
            {
                headAxisValue = CustomInput.CustomInput.GetAxis("Turn");
            }
        }

    }






    //for debugging
    /*
    public float positionXTete;
    public float limiteGauche;
    public float limiteDroite;
    */

    // Start is called before the first frame update
    /*void Start()
    {
        CustomInput.CustomInput.AddAxis("Vertical",0); //ajout de l'axe vertical
    }*/

    // Update is called once per frame
    void UpdateHeadAxis()
    {
        int camWidth = cameraWidth;
        float limitLeft = camWidth * ((1.0f - rapportZoneCentrale) * 0.5f);
        float limitRight = camWidth - limitLeft;

        Rect Head = gameObject.GetComponent<HeadTracking>().head;

        if (gameObject.GetComponent<HeadTracking>().headTrackedOnce)
        {
            if ((Head.x + (Head.x + Head.width*0.5f))< limitLeft)
            {
                CustomInput.CustomInput.SetAxis("Vertical", -1); //turn left
            }else if ((Head.x + (Head.x + Head.width * 0.5f)) > limitRight)
            {
                CustomInput.CustomInput.SetAxis("Vertical", 1); //turn left
            }
            else
            {
                CustomInput.CustomInput.SetAxis("Vertical", 0); //center
            }
        }
        //for debugging
        headAxisValue = CustomInput.CustomInput.GetAxis("Vertical");
        positionXTete = (Head.x + (Head.x + Head.width * 0.5f));
        limiteDroite = limitRight;
        limiteGauche = limitLeft;
    }
}
