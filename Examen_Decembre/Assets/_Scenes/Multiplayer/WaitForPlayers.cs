﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

[NetworkSettings(channel = 1)]
public class WaitForPlayers : NetworkBehaviour
{

    private float RetroCarSpeed;
    private bool Playing;
    private bool InstantiatedObstacle;
    public float TimeBeforeStart = 5.0f;

    void Start()
    {
        RetroCarSpeed = gameObject.GetComponent<Car_followingPath>().startSpeed;
        gameObject.GetComponent<Car_followingPath>().speed = 0;
        //CmdStopMoving();
    }

    void Update()
    {
        if (isServer && !Playing && GameObject.FindObjectOfType<NetworkManager>().GetComponent<NetworkManager>().numPlayers >= GameObject.FindObjectOfType<NetworkManager>().GetComponent<NetworkManager>().maxConnections)
        {
            //gameObject.GetComponent<NetworkInstantiate>().InstantiateObstacles();
            
            InstantiatedObstacle = true;
        }
        
        if (isServer && InstantiatedObstacle)
        {
            TimeBeforeStart -= Time.deltaTime;
        }

        if(isServer && !Playing && InstantiatedObstacle && TimeBeforeStart<0)
        {
            CmdStartMoving();
            Playing = true;
        }
    }

    // Send position to the server and run the RPC for everyone, including the server. 
    [Command]
    protected void CmdStartMoving()
    {
        RpcStartMoving();
    }

    // For each player, transfer the position from the server to the client, and set it as long as it's not the local player. 
    [ClientRpc]
    void RpcStartMoving()
    {
        Debug.Log("Strat moving instruction get");
        GameObject[] players= GameObject.FindGameObjectsWithTag("Player");
        foreach(GameObject player in players){
            player.GetComponent<Car_followingPath>().speed = RetroCarSpeed;
            player.GetComponent<Car_followingPath>().actualPath = player.GetComponent<Car_followingPath>().startPath;
        }
    }

    // Send position to the server and run the RPC for everyone, including the server. 
    [Command]
    protected void CmdStopMoving()
    {
        Debug.Log("CmdStopMoving");
        RpcStopMoving();
    }

    // For each player, transfer the position from the server to the client, and set it as long as it's not the local player. 
    [ClientRpc]
    void RpcStopMoving()
    {
        //Debug.Log("RpcStopMoving");
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            //Debug.Log("speed :" + player.GetComponent<Car_followingPath>().speed);
            player.GetComponent<Car_followingPath>().speed = 0;
            //Debug.Log("speed :" + player.GetComponent<Car_followingPath>().speed);

        }
    }
}