﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

[NetworkSettings(channel = 3)]
public class DestroyNetworkObject : NetworkBehaviour
{
    void Start()
    {
        
    }

    void Update()
    {
        
    }
    // Send position to the server and run the RPC for everyone, including the server. 
    [Command]
    public void CmdDestroyObstacle(GameObject objet)
    {

        RpcDestroyObject(objet);
    }

    // For each player, transfer the position from the server to the client, and set it as long as it's not the local player. 
    [ClientRpc]
    public void RpcDestroyObject(GameObject objet)
    {
        Destroy(objet);
    }


}

