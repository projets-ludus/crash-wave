﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

[NetworkSettings(channel = 0)]
public class SyncPosition : NetworkBehaviour
{

    //private GameObject playerBody;
    private Rigidbody physicsRoot;

    void Start()
    {
        //playerBody = transform.GetChild(0).gameObject;
        physicsRoot = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (isLocalPlayer)
        {
            Debug.Log("Is sending update :");
            CmdSyncPos(gameObject,transform.localPosition, transform.localRotation, physicsRoot.velocity);

            if (gameObject.GetComponent<Car_followingPath>().hasWin && !gameObject.GetComponent<Car_followingPath>().hasLost)
            {
                CmdWin(gameObject); //transmet la référence de la voiture qui a gagné.
            }
        }


    }

    // Send position to the server and run the RPC for everyone, including the server. 
    [Command]
    protected void CmdSyncPos(GameObject car, Vector3 localPosition, Quaternion localRotation, Vector3 velocity)
    {
        RpcSyncPos(car, localPosition, localRotation,  velocity);
    }

    // For each player, transfer the position from the server to the client, and set it as long as it's not the local player. 
    [ClientRpc]
    void RpcSyncPos(GameObject car, Vector3 localPosition, Quaternion localRotation,  Vector3 velocity)
    {
        Debug.Log("Is updating positions :");
        if (!gameObject.GetComponent<NetworkIdentity>().isLocalPlayer)
        {

            car.transform.localPosition = localPosition;
            car.transform.localRotation = localRotation;

            car.GetComponent<Rigidbody>().velocity = velocity;
            car.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);

        }
    }

    //Set who has win :
    [Command]
    protected void CmdWin(GameObject car)
    {
        RpcWin(car);
    }

    [ClientRpc]
    void RpcWin(GameObject Car)
    {

        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            if(Car != gameObject)
            {
                player.GetComponent<Car_followingPath>().hasLost = true;
            }
            else
            {
                player.GetComponent<Car_followingPath>().hasWin = true;
            }
        }
    }
}