﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

[NetworkSettings(channel = 3)]
public class NetworkInstantiate : NetworkBehaviour
{
    void Start()
    {
    }

    void Update()
    {

    }

    public void InstantiateObstacles()
    {

        GameObject[] objets = GameObject.FindGameObjectsWithTag("Obs");
        foreach(GameObject objet in objets)
        {
            NetworkServer.Spawn(objet);
            Debug.Log("Object spawned");
        }

    }


}

