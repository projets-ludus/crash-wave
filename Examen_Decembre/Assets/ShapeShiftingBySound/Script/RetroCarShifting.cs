﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RetroCarShifting : MonoBehaviour
{
    public Material shapeShiftingMaterial;
    //public GameObject shapeShiftingMesh;
    [SerializeField] public int nSample;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
     
        shapeShiftingMaterial.color = new Color(0+(AudioP.fSamples[nSample] * 150),0, 255-(AudioP.fSamples[nSample] * 150 ));
     
        // shapeShiftingMesh.transform.localScale = new Vector3(/*(AudioP.fSamples[nSample] * 0.75f  + 0.6f)*/1, /*(AudioP.fSamples[nSample] * 0.75f + 0.5f)*/1 ,(AudioP.fSamples[nSample] * 0.5f + 0.9f));
    }
}
