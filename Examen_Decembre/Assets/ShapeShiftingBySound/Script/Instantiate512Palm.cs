﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiate512Palm : MonoBehaviour
{
    public GameObject sampleCubePrefab;
    GameObject[] sampleCube = new GameObject[512];
    public float fMaxScale;

    // Start is called before the first frame update
    void Start()
    {
        for(int nCpt = 0; nCpt < 4; nCpt++)
        {
            GameObject instanceSampleCube = (GameObject)Instantiate(sampleCubePrefab);
            instanceSampleCube.transform.position = this.transform.position;
            instanceSampleCube.transform.parent = this.transform;
            instanceSampleCube.name = "SampleCube" + nCpt;
            this.transform.eulerAngles = new Vector3(0, -0.703215f * nCpt, 0);
            instanceSampleCube.transform.position = Vector3.forward * 100;
            sampleCube[nCpt] = instanceSampleCube;
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int nCpt = 0; nCpt < 4; nCpt++)
        {
            if (sampleCube != null)
            {
                sampleCube[nCpt].transform.localScale = new Vector3((AudioP.fSamples[nCpt] * fMaxScale/2)+1, (AudioP.fSamples[nCpt] * fMaxScale/2) + 1, (AudioP.fSamples[nCpt] * fMaxScale/2) + 1);
            }
        }
    }
}
