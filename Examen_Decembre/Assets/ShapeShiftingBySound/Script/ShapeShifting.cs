﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeShifting : MonoBehaviour
{
    public GameObject shapeShiftingMesh;
    [SerializeField] public int nSample;

    // Start is called before the first frame update
    void Start()
    {
        shapeShiftingMesh = gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        shapeShiftingMesh.transform.localScale = new Vector3(/*(AudioP.fSamples[nSample] * 0.75f  + 0.6f)*/0.8f, /*(AudioP.fSamples[nSample] * 0.75f + 0.5f)*/0.8f ,(AudioP.fSamples[nSample] * 0.75f + 0.8f));
    }
}
