﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomInput;

public class MovementTemp : MonoBehaviour
{
    [Header("--- Paths ---")]
    //Les gameobjects ayant pour enfants les transform de points de contrôle
    public GameObject path1;
    public GameObject path2;
    public GameObject path3;
    public GameObject path4;

    //Chemin courant
    private List<Transform> currentPath = new List<Transform>();

    //Chemin de début
    [Header("--- Position de départ ---")]
    public int startPath = 1;

    //Les listes contenant les points 
    private List<Transform> lPoints_1 = new List<Transform>();
    private List<Transform> lPoints_2 = new List<Transform>();
    private List<Transform> lPoints_3 = new List<Transform>();
    private List<Transform> lPoints_4 = new List<Transform>();

    //Tableau des listes
    private List<Transform>[] tabLists = new List<Transform>[4];



    //Liste courante, utilisée pour se déplacer actuellement, réaffectée pour changer de chemin
    private List<Transform> lcurrent = new List<Transform>();

    //Vitesse d'une voiture
    public float speed = 50f;

    //Point de contrôle actuel
    private int current;

    //Vitesse de virage
    private float damping = 5;

    //Rigidbody d'une voiture
    private Rigidbody rb;

    //Booléen pour controller au clavier
    public bool ControllableMain = true;

    //Index de chemin
    private int actualPath = 0;


    GameObject[] paths = new GameObject[4];



    // Start is called before the first frame update
    void Start()
    {

        //Récupère la position des paths
        paths = GameObject.FindGameObjectsWithTag("Path");
        path1 = paths[0];
        path2 = paths[1];
        path3 = paths[2];
        path4 = paths[3];

        //Récupère le Rigidbody du joueur
        rb = GetComponent<Rigidbody>();

        //Méthode d'initialisation des listes
        InitPathLists();

        //Initialise un tableau de listes, contenant les chemins pour manipulation ultérieure
        InitTab();

        //Au début, le chemin de départ est celui courant
        lcurrent = tabLists[startPath - 1];

        //Position de la voiture au départ
        transform.position = new Vector3(lcurrent[0].position.x, lcurrent[0].position.y, lcurrent[0].position.z);

        //Le chemin actuel prend la valeur du chemin de départ au lancement
        actualPath = startPath;

    }
    float Timer = 0f;
    public float InputDurationInSecond = 1;

    // Update is called once per frame
    void Update()
    {
        //Si le prefab correspond à celui du joueur.
       
            //-----------------------------------------------------------//
            //------------------ Système d'inputs -----------------------//
            //-----------------------------------------------------------//

            if (ControllableMain)
            {
                InputMain();
            }

            if (Timer < InputDurationInSecond)
            {
                Timer += Time.deltaTime;
            }
            else
            {
                switch (CustomInput.CustomInput.GetAxis("Vertical"))
                {
                    case -1:
                        GoingLeft();
                        break;
                    case 1:
                        GoingRight();
                        break;
                    case 0:
                        break;

                    default:
                        break;
                }
                Timer = 0f;
            }

            //-----------------------------------------------------------//
            //------------------ Gère le chemin choisi ------------------//
            //-----------------------------------------------------------//

            //Si la distance est inferieure à 2 unités
            if (Vector3.Distance(transform.position, lcurrent[current].position) < 2f)
            {
                //et si le joueur n'est pas au dernier point de contrôle
                if (current != lcurrent.ToArray().Length - 1)
                {
                    //Incrémente l'objectif, donc le prochain point de contrôle
                    current += 1;
                }
            }
            else
            {
                //Fait avancer le joueur vers l'objectif
                Vector3 pos = Vector3.MoveTowards(transform.position, lcurrent[current].position, speed * Time.deltaTime);
                rb.MovePosition(pos);

            }




           //------------------------------------------------//
        

    }



    private void LateUpdate()
    {
        //Rotation de la voiture en fonction de son prochain objectif
        var rotation = Quaternion.LookRotation(lcurrent[current].position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);


    }
    void InitPathLists()
    {
        //Boucle qui ajoute tous les points du premier chemin dans une liste
        foreach (Transform child in path1.transform)
        {
            lPoints_1.Add(child);
        }

        //Boucle qui ajoute tous les points du deuxième chemin dans une liste
        foreach (Transform child in path2.transform)
        {
            lPoints_2.Add(child);
        }

        //Boucle qui ajoute tous les points du troisième chemin dans une liste
        foreach (Transform child in path3.transform)
        {
            lPoints_3.Add(child);
        }

        //Boucle qui ajoute tous les points du quatrième chemin dans une liste
        foreach (Transform child in path4.transform)
        {
            lPoints_4.Add(child);
        }
    }

    void InitTab()
    {
        tabLists[0] = lPoints_1;
        tabLists[1] = lPoints_2;
        tabLists[2] = lPoints_3;
        tabLists[3] = lPoints_4;
    }



    void GoingLeft()
    {
        if (lcurrent != tabLists[0])
        {
            actualPath--;
            lcurrent = tabLists[actualPath];
        }
    }

    void GoingRight()
    {
        if (lcurrent != tabLists[tabLists.Length - 1])
        {
            actualPath++;
            lcurrent = tabLists[actualPath];
        }
    }


    void InputMain()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            lcurrent = tabLists[0];
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            lcurrent = tabLists[1];
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            lcurrent = tabLists[2];
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            lcurrent = tabLists[3];
        }
    }
}