﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsOnPlayer : MonoBehaviour
{

    public TrailRenderer left;
    public TrailRenderer right;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(GetComponent<OnEnter>().bonusTouched == true)
        {
            ActivateTrails();
        }
        else
        {
            DeactivateTrails();
        }
        //---------------------------------------------//
        if (GetComponent<OnEnter>().malusTouched == true)
        {
            //Effets quand un malus est touché
        }
        else
        {
            //Inverse des effets
        }

    }

    void ActivateTrails()
    {
        left.enabled = true;
        right.enabled = true;
    }
    void DeactivateTrails()
    {
        left.enabled = false;
        right.enabled = false;
    }
}
