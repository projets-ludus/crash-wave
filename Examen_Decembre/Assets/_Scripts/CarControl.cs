﻿using UnityEngine;
using System.Collections;

public class CarControl : MonoBehaviour
{

    // PUBLIC

    // Wheel Wrapping Objects
    [Header("--- Transform Wrapper ---")]
    public Transform frontLeftWheelWrapper;
    public Transform frontRightWheelWrapper;
    public Transform rearLeftWheelWrapper;
    public Transform rearRightWheelWrapper;

    // Wheel Meshes
    // Front
    [Header("--- Wheel meshes ---")]
    public Transform frontLeftWheelMesh;
    public Transform frontRightWheelMesh;
    // Rear
    public Transform rearLeftWheelMesh;
    public Transform rearRightWheelMesh;

    // Wheel Colliders
    // Front
    [Header("--- Wheel colliders ---")]
    public WheelCollider wheelFL;
    public WheelCollider wheelFR;
    // Rear
    public WheelCollider wheelRL;
    public WheelCollider wheelRR;

    [Header("Setttings")]
    public float maxTorque = 50f;
    public float brakeTorque = 100f;

    // max wheel turn angle;
    public float maxWheelTurnAngle = 50f; // degrees

    // car's center of mass
    public Vector3 centerOfMass = new Vector3(0f, 0f, 0f); // unchanged

    // GUI
    //...
    public float RO_speed; // DISPLAY ONLY
    public float RO_EngineTorque; // DISPLAY ONLY
    public float RO_SteeringAngleFL; // DISPLAY ONLY
    public float RO_SteeringAngleFR; // DISPLAY ONLY
    public float RO_BrakeTorque; // DISPLAY ONLY

    // PRIVATE
    private Rigidbody rb;

    // acceleration increment counter
    private float torquePower = 50f;

    // turn increment counter
    private float steerAngle = 0f;

    // original wheel positions
    // Front Left
    private float wheelMeshWrapperFLx;
    private float wheelMeshWrapperFLy;
    private float wheelMeshWrapperFLz;
    // Front Right
    private float wheelMeshWrapperFRx;
    private float wheelMeshWrapperFRy;
    private float wheelMeshWrapperFRz;
    // Rear Left
    private float wheelMeshWrapperRLx;
    private float wheelMeshWrapperRLy;
    private float wheelMeshWrapperRLz;
    // Rear Right
    private float wheelMeshWrapperRRx;
    private float wheelMeshWrapperRRy;
    private float wheelMeshWrapperRRz;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = centerOfMass;


        // Setup initial values

        // Front Left
        wheelMeshWrapperFLx = frontLeftWheelWrapper.localPosition.x;
        wheelMeshWrapperFLy = frontLeftWheelWrapper.localPosition.y;
        wheelMeshWrapperFLz = frontLeftWheelWrapper.localPosition.z;
        // Front Right
        wheelMeshWrapperFRx = frontRightWheelWrapper.localPosition.x;
        wheelMeshWrapperFRy = frontRightWheelWrapper.localPosition.y;
        wheelMeshWrapperFRz = frontRightWheelWrapper.localPosition.z;
        // Rear Left
        wheelMeshWrapperRLx = rearLeftWheelWrapper.localPosition.x;
        wheelMeshWrapperRLy = rearLeftWheelWrapper.localPosition.y;
        wheelMeshWrapperRLz = rearLeftWheelWrapper.localPosition.z;
        // Rear Right
        wheelMeshWrapperRRx = rearRightWheelWrapper.localPosition.x;
        wheelMeshWrapperRRy = rearRightWheelWrapper.localPosition.y;
        wheelMeshWrapperRRz = rearRightWheelWrapper.localPosition.z;
    }


    // Visual updates
    void Update()
    {

        // SETUP WHEEL MESHES

        // Turn the mesh wheels
        frontLeftWheelWrapper.localEulerAngles = new Vector3(0, steerAngle, 0);
        frontRightWheelWrapper.localEulerAngles = new Vector3(0, steerAngle, 0);

        // Wheel rotation
  
        frontLeftWheelMesh.Rotate(0, wheelFL.rpm / 60 * 360 * Time.deltaTime, 0);
        frontRightWheelMesh.Rotate(0, wheelFR.rpm / 60 * 360 * Time.deltaTime, 0);
        rearLeftWheelMesh.Rotate(-(wheelRL.rpm / 60 * 360 * Time.deltaTime), 0, 0);
        rearRightWheelMesh.Rotate(-(wheelRL.rpm / 60 * 360 * Time.deltaTime), 0, 0);

        // Audio
        //audio.pitch = (torquePower / maxTorque) + 0.5f;
    }

    // Physics updates
    void FixedUpdate()
    {

        // CONTROLS - FORWARD & RearWARD
        if (Input.GetKey(KeyCode.Space))
        {
            // BRAKE
            torquePower = 0f;
            wheelRL.brakeTorque = brakeTorque;
            wheelRR.brakeTorque = brakeTorque;
        }
        else
        {
            // SPEED
            torquePower = maxTorque * Input.GetAxisRaw("Vertical");
            wheelRL.brakeTorque = 0f;
            wheelRR.brakeTorque = 0f;

        }
        // Apply torque
        wheelRR.motorTorque = torquePower;
        wheelRL.motorTorque = torquePower;

        /*
        // Debug.Log(Input.GetAxis("Vertical"));
        Debug.Log("torquePower: " + torquePower);
        Debug.Log("brakeTorque RL: " + wheelRL.brakeTorque);
        Debug.Log("brakeTorque RR: " + wheelRR.brakeTorque);
        Debug.Log("steerAngle: " + steerAngle);
        */

        // CONTROLS - LEFT & RIGHT
        // apply steering to front wheels
        steerAngle = maxWheelTurnAngle * Input.GetAxis("Horizontal");
        wheelFL.steerAngle = steerAngle;
        wheelFR.steerAngle = steerAngle;

        // Debug info
        RO_BrakeTorque = wheelRL.brakeTorque;
        RO_SteeringAngleFL = wheelFL.steerAngle;
        RO_SteeringAngleFR = wheelFR.steerAngle;
        RO_EngineTorque = torquePower;

        // SPEED
        // debug info

        RO_speed = rb.velocity.magnitude;
    }
}