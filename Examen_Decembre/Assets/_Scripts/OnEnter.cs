﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnter : MonoBehaviour
{
    //********************************//
    //--------- PARAMÈTRES -----------//
    //********************************//

    //Vitesse de base de la voiture
    private float normalSpeed;

    //Vitesse quand voiture ralentie
    private float speedTemp = 5f;

    //Vitesse de boost de voiture
    private float speedBoost = 50f;

    [HideInInspector] public bool bonusTouched = false;
    [HideInInspector] public bool malusTouched = false;

    //********************************//

    private void Start()
    {
        normalSpeed = GetComponent<Car_followingPath>().startSpeed;
    }


    private void OnTriggerEnter(Collider collision)
    {
        //Effets de l'obstacle
        if(collision.gameObject.CompareTag("Obs"))
        {
            malusTouched = true;
            GetComponent<Car_followingPath>().speed = 5f;
            StartCoroutine(wait(collision));
            //gameObject.GetComponent<DestroyNetworkObject>().CmdDestroyObstacle(collision.gameObject);
        }

        //Effets du bonus
        if (collision.gameObject.CompareTag("Bonus"))
        {
            bonusTouched = true;
            GetComponent<Car_followingPath>().speed += speedBoost;
            StartCoroutine(wait(collision));
            //gameObject.GetComponent<DestroyNetworkObject>().CmdDestroyObstacle(collision.gameObject);
        }


    }

    IEnumerator wait(Collider collision)
    {
        //Attend deux secondes et réassigne la vitesse de base
        yield return new WaitForSeconds(3f);
        GetComponent<Car_followingPath>().speed = normalSpeed;
        malusTouched = false;
        bonusTouched = false;

    }

}
