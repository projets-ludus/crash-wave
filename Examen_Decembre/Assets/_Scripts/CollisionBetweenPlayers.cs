﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

[NetworkSettings(channel = 2)]
public class CollisionBetweenPlayers : NetworkBehaviour
{


    //private GameObject playerBody;
    private Rigidbody physicsRoot;

    void Start()
    {
        //playerBody = transform.GetChild(0).gameObject;
        physicsRoot = GetComponent<Rigidbody>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (isLocalPlayer && other.transform.CompareTag("Player"))
        {
            if (other.transform.GetComponent<Car_followingPath>().actualPath > gameObject.transform.GetComponent<Car_followingPath>().actualPath)
            {
                CmdMoveCar(other.gameObject, 1);
                gameObject.transform.GetComponent<Car_followingPath>().GoingLeft();
            }
            else
            {
                CmdMoveCar(other.gameObject, -1);
                gameObject.transform.GetComponent<Car_followingPath>().GoingRight();
            }

        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Physics.IgnoreCollision(collision.collider, gameObject.GetComponent<Collider>());
        }
    }


    //Set who has win :
    [Command]
    protected void CmdMoveCar(GameObject car, float move)
    {
        RpcMoveCar(car, move);
    }

    [ClientRpc]
    void RpcMoveCar(GameObject Car, float move)
    {
        if (isLocalPlayer)
        {
            if (move == -1)
            {
                Car.GetComponent<Car_followingPath>().GoingLeft();
            }
            if (move == 1)
            {
                Car.GetComponent<Car_followingPath>().GoingRight();
            }
        }  
    }
}