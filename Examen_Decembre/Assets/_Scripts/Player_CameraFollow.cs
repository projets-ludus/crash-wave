﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_CameraFollow : MonoBehaviour
{
    public GameObject player;

    public float distancePlayerCamera;
    private Quaternion cameraRotation = new Quaternion();
    Vector3 pos = new Vector3();

    private void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        pos = new Vector3(player.transform.rotation.x + 8.5f, transform.rotation.y, transform.rotation.z);
        cameraRotation.eulerAngles = pos;
        transform.rotation = cameraRotation;
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 3, player.transform.position.z - 8.79f);

    }
}
