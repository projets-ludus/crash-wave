﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ObstacleSpawner : MonoBehaviour
{

    

    [Header("--- Paths ---")]
    //Les gameobjects ayant pour enfants les transform de points de contrôle
    //public GameObject path1;
    //public GameObject path2;
    //public GameObject path3;
    //public GameObject path4;
    
    //Les listes contenant les points 
    //private List<Transform> lPoints_1 = new List<Transform>();
    //private List<Transform> lPoints_2 = new List<Transform>();
    //private List<Transform> lPoints_3 = new List<Transform>();
    //private List<Transform> lPoints_4 = new List<Transform>();

    private List<Transform>[] tabLists = new List<Transform>[4];


    //******************************************//
    //--------------- Paramètres ---------------//
    //******************************************//

    //Espace entre chaque obstacle (ligne)
    private int espaceInterObstacles = 4;

    //Espace entre chaque bonus (ligne)
    private int espaceEntreBonus = 10;

    //Prefab d'obstacle à instancier
    public GameObject obstacle;

    //Prefabs de bonus à instancier
    public GameObject bonus;

    //******************************************//





    private void Start()
    {
        if(gameObject.GetComponent<NetworkIdentity>().isServer){

            //Initialise les lists et tab
            //InitPathLists();
            InitTab();

            //Calcul le total de transform dans la carte
            int i;
            int total =0;
            for (i=0; i< tabLists.Length; i++)
            {
                total += tabLists[i].Count;
            }

            //Divise le total par 4 pour retourner le nombre de transform par colonne
            total = total / 4;

            //Boucle d'instanciation d'obstacle
            for(i = 0; i < total; i++)
            {
                //Instancie un obstacle tous les espaceInterObstacles, sauf aux positions de départ
                if (i % espaceEntreBonus == 0 && i != 0 && i != 1 && i != 2 && i != 3)
                {
                    //random de colonne
                    int random1 = Random.Range(0, tabLists.Length-1);

                    //Fait apparaitre un objet aux coordonées : [Colonne][i].position
                    GameObject exp = Instantiate(bonus, tabLists[random1][i].position, Quaternion.identity);
                    NetworkServer.Spawn(exp);
                }
                else //Instancie un obstacle tous les espaceInterObstacles, sauf aux positions de départ
                if (i % espaceInterObstacles == 0 && i != 0 && i != 1 && i != 2 && i != 3)
                {
                    //random de colonne
                    int random1 = Random.Range(0, tabLists.Length - 1);

                    //Fait apparaitre un objet aux coordonées : [Colonne][i].position
                    GameObject exp = Instantiate(obstacle, tabLists[random1][i].position, Quaternion.identity);
                    NetworkServer.Spawn(exp);
                }
            }
        }
    }



    void InitTab()
    {
        GameObject[] paths = GameObject.FindGameObjectsWithTag("Path");
        int i = 0;
        //int j = 0;
        foreach(GameObject path in paths)
        {
            tabLists[i] = new List<Transform> { };
            foreach (Transform child in path.transform)
            {
                tabLists[i].Add(child);
            }
            i++;
            
        }
    }



    /*void InitPathLists()
    {
        //Boucle qui ajoute tous les points du premier chemin dans une liste
        foreach (Transform child in path1.transform)
        {
            lPoints_1.Add(child);
        }

        //Boucle qui ajoute tous les points du deuxième chemin dans une liste
        foreach (Transform child in path2.transform)
        {
            lPoints_2.Add(child);
        }

        //Boucle qui ajoute tous les points du troisième chemin dans une liste
        foreach (Transform child in path3.transform)
        {
            lPoints_3.Add(child);
        }

        //Boucle qui ajoute tous les points du quatrième chemin dans une liste
        foreach (Transform child in path4.transform)
        {
            lPoints_4.Add(child);
        }
    }*/
}
