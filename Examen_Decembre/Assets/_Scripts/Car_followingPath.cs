﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomInput;
using UnityEngine.Networking;

public class Car_followingPath : MonoBehaviour
{
    [Header("--- Paths ---")]
    //Les gameobjects ayant pour enfants les transform de points de contrôle
    public GameObject path1;
    public GameObject path2;
    public GameObject path3;
    public GameObject path4;

    //Chemin courant
    //private List<Transform> currentPath = new List<Transform>();

    //Chemin de début
    [Header("--- Position de départ ---")]
    public int startPath;

    //Les listes contenant les points 
    private readonly List<Transform> lPoints_1 = new List<Transform>();
    private readonly List<Transform> lPoints_2 = new List<Transform>();
    private readonly List<Transform> lPoints_3 = new List<Transform>();
    private readonly List<Transform> lPoints_4 = new List<Transform>();

    //Tableau des listes
    private readonly List<Transform>[] tabLists = new List<Transform>[4];

    //gestion du nombre de tour
    public int nBTour = 1; // numéro du tour
    public int MaxTour = 3; // nombre de tour max.

    //gestion de la victoire
    public bool hasWin = false;
    public bool hasLost = false;

    //Liste courante, utilisée pour se déplacer actuellement, réaffectée pour changer de chemin
    private List<Transform> lcurrent = new List<Transform>();

    //Vitesse d'une voiture
    public float speed = 50f;
    public float startSpeed = 50f;

    //Point de contrôle actuel
    private int current;

    //Vitesse de virage
    private readonly float damping = 5;

    //Rigidbody d'une voiture
    private Rigidbody rb;

    //Booléen pour controller au clavier
    public bool ControllableMain;

    //Index de chemin
    public int actualPath = 0;


    //Effet de victoire
    public ParticleSystem victory;

    // Start is called before the first frame update
    void Start()
    {
        //défini le numéro du tour à 1;
        nBTour = 1;

        //Vérifie si la voiture appartient au joueur et active les composants
        startPath = GameObject.FindGameObjectsWithTag("Player").Length - 1;
        //startPath = (int) gameObject.GetComponent<NetworkIdentity>().netId.Value -1;
        if (gameObject.GetComponent<NetworkIdentity>().isLocalPlayer)
        {
            gameObject.GetComponentInChildren<Camera>().enabled = true;
            gameObject.GetComponentInChildren<AudioListener>().enabled = true;
            gameObject.GetComponentInChildren<Rigidbody>().useGravity = true;
        }

        //Récupère la position des paths
        GameObject[] paths = GameObject.FindGameObjectsWithTag("Path");
        path1 = paths[0];
        path2 = paths[1];
        path3 = paths[2];
        path4 = paths[3];

        //Récupère le Rigidbody du joueur
        rb = GetComponent<Rigidbody>();

        //Méthode d'initialisation des listes
        InitPathLists();

        //Initialise un tableau de listes, contenant les chemins pour manipulation ultérieure
        InitTab();

        //Le chemin actuel prend la valeur du chemin de départ au lancement
        actualPath = startPath;


        //Au début, le chemin de départ est celui courant
        lcurrent = tabLists[actualPath];

        if (gameObject.GetComponent<NetworkIdentity>().isLocalPlayer)
        {
            //Position de la voiture au départ
            transform.position = new Vector3(lcurrent[0].position.x, lcurrent[0].position.y, lcurrent[0].position.z);
        }


    }
    float Timer = 0f;
    public float InputDurationInSecond = 1;

    bool done = false;

    // Update is called once per frame
    void Update()
    {

        if(hasWin && done == false)
        {
            ParticleSystem exp = Instantiate(victory, transform.position, Quaternion.identity);
            done = true;
        }

        //Si le prefab correspond à celui du joueur.
        if (gameObject.GetComponent<NetworkIdentity>().isLocalPlayer)
        {
            Debug.Log("Is Local Player");
            //-----------------------------------------------------------//
            //------------------ Système d'inputs -----------------------//
            //-----------------------------------------------------------//

            if (ControllableMain)
            {
                InputMain();
            }

            if (Timer < InputDurationInSecond)
            {
                Timer += Time.deltaTime;
            }
            else
            {
                switch (CustomInput.CustomInput.GetAxis("Vertical"))
                {
                    case -1:
                        GoingLeft();
                        break;
                    case 1:
                        GoingRight();
                        break;
                    case 0:
                        break;

                    default:
                        break;
                }
                Timer = 0f;
            }


            //-----------------------------------------------------------//
            //------------------ Gère le chemin choisi ------------------//
            //-----------------------------------------------------------//
            lcurrent = tabLists[actualPath];

            //Si la distance est inferieure à 2 unités
            //if (Vector3.Distance(transform.position, lcurrent[current].position) < 2f)
            if (Mathf.Sqrt(Mathf.Pow(transform.position.x - lcurrent[current].position.x, 2f) + Mathf.Pow(transform.position.z - lcurrent[current].position.z, 2f)) < 2f)
            {

                //et si le joueur n'est pas au dernier point de contrôle
                if (current != lcurrent.Count - 1)
                {
                    //Incrémente l'objectif, donc le prochain point de contrôle
                    current += 1;
                }
                else
                {
                    if (nBTour < MaxTour)
                    {
                        nBTour++;
                        current = 0;
                    }
                    else
                    {
                        if (!hasLost)
                        {
                            hasWin = true;
                        }
                    }

                }
            }
            else
            {
                //Fait avancer le joueur vers l'objectif
                Vector3 pos = Vector3.MoveTowards(transform.position, new Vector3(lcurrent[current].position.x, transform.position.y, lcurrent[current].position.z), speed * Time.deltaTime);
                rb.MovePosition(pos);

            }




            //------------------------------------------------//

        }
    }



    private void LateUpdate()
    {
        //Rotation de la voiture en fonction de son prochain objectif
        var rotation = Quaternion.LookRotation(lcurrent[current].position - transform.position);
        rotation.x = transform.rotation.x;
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);


    }
    void InitPathLists()
    {
        //Boucle qui ajoute tous les points du premier chemin dans une liste
        foreach (Transform child in path1.transform)
        {
            lPoints_1.Add(child);
        }

        //Boucle qui ajoute tous les points du deuxième chemin dans une liste
        foreach (Transform child in path2.transform)
        {
            lPoints_2.Add(child);
        }

        //Boucle qui ajoute tous les points du troisième chemin dans une liste
        foreach (Transform child in path3.transform)
        {
            lPoints_3.Add(child);
        }

        //Boucle qui ajoute tous les points du quatrième chemin dans une liste
        foreach (Transform child in path4.transform)
        {
            lPoints_4.Add(child);
        }
    }

    void InitTab()
    {
        tabLists[0] = lPoints_1;
        tabLists[1] = lPoints_2;
        tabLists[2] = lPoints_3;
        tabLists[3] = lPoints_4;
    }



    public void GoingLeft()
    {
        if (lcurrent != tabLists[0])
        {
            actualPath--;
            lcurrent = tabLists[actualPath];
            Debug.Log("Going left");

        }
    }

    public void GoingRight()
    {
        if (lcurrent != tabLists[tabLists.Length - 1])
        {
            actualPath++;
            lcurrent = tabLists[actualPath];
            Debug.Log("Going right");

        }
    }


    void InputMain()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            actualPath = 0;
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            actualPath = 1;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            actualPath = 2;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            actualPath = 3;
        }
    }

}