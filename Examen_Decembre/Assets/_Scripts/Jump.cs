﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using CustomInput;
public class Jump : MonoBehaviour
{
    public float jumpScale = 1;
    public bool isJumping;
    // Start is called before the first frame update
    void Start()
    {
        isJumping = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<NetworkIdentity>().isLocalPlayer)
        {
            if (!isJumping && (Input.GetKeyDown(KeyCode.Space)||CustomInput.CustomInput.GetAxis("Jump")==1))
            {
                isJumping = true;
                Rigidbody body = gameObject.transform.GetComponent<Rigidbody>();
                body.AddForce(new Vector3(0, jumpScale * 5000, 0), ForceMode.Impulse);
                Debug.Log("Jump");
            }
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        try
        {
            if (isJumping && collision.transform.parent.parent.CompareTag("Terrain"))
            {
                isJumping = false;
            }
        }
        catch
        {

        }
        

    }
}
